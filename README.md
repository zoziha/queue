# Queue in Fortran

Queue container in Fortran.

Queues are suitable for adding indeterminate data (especially when the length is difficult to determine by other means).

*Suggestions and code contributions are welcome.*

```fortran
use queue_m, only: queue_t
```

## Build with [fortran-lang/fpm](https://github.com/fortran-lang/fpm)

Support compilers: `gfortran`, `ifort`.

```sh
fpm run --example --list
```

```toml
[dependencies]
queue = { git = "https://gitee.com/zoziha/queue.git" }
```

## Reference

- [zoziha/dlinked_list](https://gitee.com/zoziha/dlinked_list)
- [zoziha/slinked_list](https://gitee.com/zoziha/slinked_list)
- [fortran-lang/stdlib](https://github.com/fortran-lang/stdlib)
- [fortran-fans/forlab](https://github.com/fortran-fans/forlab)
- [浅谈单链表与双链表的区别](https://blog.csdn.net/kangxidagege/article/details/80211225)
