program main

    use queue_m, only: queue_t
    type(queue_t) :: queue
    
    call queue%enqueue(1)
    call queue%enqueue(2)
    print *, queue%size()
    call queue%dequeue()
    print *, queue%size()
    call queue%clear()
    print *, queue%size()

end program main

!           2
!           1
!           0