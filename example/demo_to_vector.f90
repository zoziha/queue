program main

    use queue_m, only: queue_t
    type(queue_t) :: queue
    class(*), allocatable :: array
    
    call queue%enqueue(1)
    call queue%enqueue(2)
    call queue%enqueue(3)
    
    do i = 1, queue%size()
        call queue%dequeue(array)
        select type (array)
        type is (integer)
            print *, array
        end select
    end do
    print *, 'size: ', queue%size()

end program main

!           1
!           2
!           3
! size:            0
